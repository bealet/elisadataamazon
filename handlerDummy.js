'use strict';

module.exports.dummy = (event, context, callback) => {

    const response = {
        statusCode: 412,
        headers: {
            'Content-Type': 'application/json; charset=utf-8',
        },
        body: JSON.stringify({
            hello: 'world'
        })
    };
    callback(null, response);
}